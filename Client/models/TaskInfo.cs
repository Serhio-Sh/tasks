﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.models
{
    public class TaskInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int PerformerId { get; set; }
        public User Performer { get; set; }
        public DateTime FinishedAt { get; set; }
        public string Description { get; set; }
    }
}
