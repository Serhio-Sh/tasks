﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.models
{
    public class ProjectInfo
    {
        public IEnumerable<TaskInfo> Tasks { get; set; }
        public User Autor { get; set; }
        public Team Team { get; set; }
        public string Description { get; set; }
    }
}
