﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WebAPI.Proj.Common.DTO;
using Client.models;
using WebAPI.Proj.DAL.Entities;
using System.Timers;
using WebAPI.Proj.DAL.Enums;

namespace Client
{
    class Program
    {

        private const string APP_PATH = "http://localhost:19847";
        static readonly HttpClient client = new();

        static async System.Threading.Tasks.Task Main(string[] args)
        {

        }

        private static Timer Timer;
        public static Task<int> MarkRandomTaskWithDelay(int interval = 1000)
        {
            var tcs = new TaskCompletionSource<int>();

            Timer = new Timer(interval);
            Timer.Elapsed += async (o, e) =>
            {
                var response = await client.GetStringAsync(APP_PATH + "/api/Tasks");
                var tasks = JsonConvert.DeserializeObject<List<WebAPI.Proj.DAL.Entities.Task>>(response);

                var rand = new Random();
                var randomValue = rand.Next(0, tasks.Count);
                tasks[randomValue].State = WebAPI.Proj.DAL.Enums.TaskState.Done;

                var stringContent = new StringContent(JsonConvert.SerializeObject(tasks[randomValue]));
                var request = await client.PutAsync(APP_PATH + "/api/Tasks", stringContent);

                tcs.SetResult(tasks[randomValue].Id);
            };

            return tcs.Task;
        }



        //Получить кол-во тасков у проекта конкретного пользователя (по id) (словарь, где ключом будет проект, а значением кол-во тасков).
        public static async Task<Dictionary<ProjectInfo, int>> GetNamberTasks(int id, IEnumerable<ProjectInfo> collection)
        {
            var v = await System.Threading.Tasks.Task.Run(() => 
                    from c in collection
                    select new
                    {
                        Project = c,
                        NumbTasks = c.Tasks.Count(x => x.Performer.Id == id)
                    });

            return v.ToDictionary(x => x.Project, x => x.NumbTasks);
        }

        //Получить список тасков, назначенных на конкретного пользователя (по id), где name таска < 45 символов (коллекция из тасков).
        public static async Task<List<TaskInfo>> GetTasks(int id, IEnumerable<ProjectInfo> collection)
        {
            var v = await System.Threading.Tasks.Task.Run(() => 
                    from c in collection
                    from t in c.Tasks
                    where t.Performer.Id == id && t.Name.Length < 45
                    select t);

            return v.ToList();
        }

        //Получить список (id, name) из коллекции тасков, которые выполнены (finished) в текущем (2021) году для конкретного пользова
        public static async Task<Dictionary<int, string>> GetFinished21(int id, IEnumerable<ProjectInfo> collection)
        {
            var v = await System.Threading.Tasks.Task.Run(() => 
                    from c in collection
                    from t in c.Tasks
                    where t.Performer.Id == id && t.FinishedAt.Year == 2021
                    select new { t.Id, t.Name });

            return v.ToDictionary(x => x.Id, x => x.Name);
        }

        //Получить список (id, имя команды и список пользователей) из коллекции команд, участники которых старше 10 лет, отсортированных по дате
        //регистрации пользователя по убыванию, а также сгруппированных по командам.
        public static async Task<Dictionary<Team, List<User>>> GetTeam_Users(IEnumerable<ProjectInfo> collection)
        {
            var v = await System.Threading.Tasks.Task.Run(() => 
                    from c in collection
                    let performers = c.Tasks.Select(x => x.Performer).Distinct()
                    where (performers.All(u => 2021 - u.BirthDay.Year > 10))
                    select new
                    {
                        Team = c.Team,
                        Users = performers.OrderByDescending(x => x.RegisteredAt).ToList()
                    });

            return v.ToDictionary(x => x.Team, x => x.Users);
        }

        //Получить список пользователей по алфавиту first_name (по возрастанию) с отсортированными tasks по длине name (по убыванию).
        public static async Task<Dictionary<User, List<WebAPI.Proj.DAL.Entities.Task>>> GetUser_Tasks(IEnumerable<User> users, IEnumerable<WebAPI.Proj.DAL.Entities.Task> tasks)
        {
            var v = await System.Threading.Tasks.Task.Run(() => 
                    from user in users
                    orderby user.FirstName
                    let task = tasks.Where(t => t.PerformerId == user.Id).OrderByDescending(t => t.Name)
                    select new 
                    {
                        User = user,
                        Tasks = tasks
                    });

            return v.ToDictionary(x => x.User, x => x.Tasks.ToList());
        }

        // 7.
        public static async Task<List<ProjectDTO_>> GetProjects(IEnumerable<ProjectInfo> collection)
        {
            var v = await System.Threading.Tasks.Task.Run(() => 
                    from c in collection
                    let ollUsers = c.Tasks.Where(t => t.PerformerId != c.Autor.Id).Select(x => x.Performer).Distinct()
                    let maxDisc = c.Tasks.FirstOrDefault(t => t.Name.Length == c.Tasks.Max(x => x.Description.Length))
                    let minName = c.Tasks.FirstOrDefault (t => t.Name.Length == c.Tasks.Min(x => x.Name.Length))
                    select new ProjectDTO_
                    {
                        Project = c,
                        LongestTask = maxDisc,
                        ShortTask = minName,
                        CountUsers = (c.Description.Length > 20 || c.Tasks.Count() < 3)
                                     ? (ollUsers.Count() + 1)// +Autor
                                     : (0)
                    });

            return v.ToList();
        }

        public class ProjectDTO_
        {
            public ProjectInfo Project { get; set; }
            public TaskInfo LongestTask { get; set; }
            public TaskInfo ShortTask { get; set; }
            public int CountUsers { get; set; }
        }
    }
}
